---
layout: default
permalink: /404.html
title: 404
---

This page does not exist. [Go back]({{ "/" | prepend: site.baseurl | replace: '//', '/' }}).

![](/assets/michi-jumping-cat.gif)
![](/assets/michi-jumping-cat-down.gif)