---
title: KMA CTF 2023 Lần 1
published: true
---
{% raw %}

| Challenge                   | Solves | Points |
| --------------------------- | ------ | ------ |
| [Vào đây!](#vào-đây)        | 22     | 100    |
| [Jo`in Le'm](#join-lem)     | 5      | 856    |
| [Flag Holder](#flag-holder) | 4      | 919    |
| [Ninja Shop](#ninja-shop)   | 2      | 991    |

![image-20230625202250031](./assets/image-20230625202250031.png)

# Vào đây!

![image-20230619132555233](./assets/image-20230619132555233.png)

Truy cập vào trang web thì mình nhận được source code

```php
const fs = require('fs')
const app = require('fastify')()
const crypto = require('crypto')
const md5 = d => crypto.createHash('md5').update(d).digest('hex')
const dbPromisePool = require('mysql2').createPool({
  host: 'mysql',
  user: 'root',
  database: 'local_db',
  password: 'local_password'
}).promise()


// app.setErrorHandler((error, req, resp) => {
//   console.error(`[fastify]`, error)
//   resp.status(503).send({ error: 'Vui lÃ²ng thá»­ láº¡i sau.' })
// })

app.addHook('preHandler', async (req, resp) => {
  resp.status(200).header('Content-Type', 'application/json')
})

app.post('/login', async req => {
  if (req.body.user === 'admin') return;
  const [rows] = await dbPromisePool.query(`select *, bio as flag from users where username = ? and password = ? limit 1`, [req.body.user, req.body.pass])
  return rows[0]
})

app.post('/register', async req => {
  const [rows] = await dbPromisePool.query(`insert users(username, password, bio) values(?, ?, ?)`, [req.body.user, md5(req.body.pass), req.body.bio])
  if (rows.insertId) return String(rows.insertId)
  return { error: 'Lá»—i, vui lÃ²ng thá»­ láº¡i sau' }
})

app.get('/', async (req, resp) => {
  resp.status(200).header('Content-Type', 'text/plain')
  return fs.promises.readFile(__filename)
})

app.listen({ port: 3000, host: '0.0.0.0' }, () => console.log('Running', app.addresses()))
```

Như trong source code đã viết thì ở bài này mình chỉ có 2 chức năng register và login. 

Chức năng register sử dụng câu truy vấn `insert users(...)` để thêm thông tin người dùng vào database với đầu vào là các giá trị `user, pass, bio` được post lên với format json

![image-20230619132917761](./assets/image-20230619132917761.png)

Còn chức năng login thì sử dụng câu truy vấn `select` để lấy dữ liệu với các dữ liệu tương ứng `user và password encrypt hash md5` 

![image-20230619133153418](./assets/image-20230619133153418.png)

Mình có để ý ở dòng này có ghi `if (req.body.user === 'admin') return;` nên mình dự đoán flag nằm ở cột bio của user admin, và dòng này mình có thể bypass được bằng cách uppercase  

![image-20230619133612030](./assets/image-20230619133612030.png)

Challenge này sử dụng mysql2 để truyền các param vào câu query cho an toàn hơn, tuy nhiên chương trình không có các thao tác kiểm tra đầu vào dữ liệu, nên việc mình có thể truyền vào các object hay một dữ liệu khác là hoàn toàn có thể

Ví dụ như khi mình truyền vào một param là một object như bên dưới thì khi đó câu query của mình sẽ là 

```sql
select *, bio as flag from users where username = longkd7199999 and password = `password` = 'abc' limit 1
```

![image-20230619135654922](./assets/image-20230619135654922.png)

Rồi mình search các tips/tricks về trường hợp này và tìm được một bài wu [google-ctf-2020/web/log-me-in.md](https://github.com/eskildsen/google-ctf-2020/blob/master/web/log-me-in.md)

Đối với trường hợp này câu truy vấn của mình sẽ kiểm tra 

```
password = `password` = 'abc'
```

thay vì kiểm tra `password = 'abc'`

```
password = `password` sẽ trả về giá trị là 1 vì đây là so sánh 2 cột với nhau -> kết quả trả về 1
rồi sau đó kiểm tra 1 = 'abc' trả về giá trị 0 vì nó sai :D 
khi này mình sửa lại abc thành 1 là kết quả của câu query sẽ là 1, login thành công mà không cần password, SQL Injection thành công
```

![image-20230619140503513](./assets/image-20230619140503513.png)

Đổi lại `"user":"Admin"` và có flag

![image-20230619140529404](./assets/image-20230619140529404.png)

`Flag: KMACTF{SQLInjection_That_La_Don_Gian}`

# Jo`in Le'm

![image-20230619172618157](./assets/image-20230619172618157.png)

> :warning:
>
> Hint 1: Kho^ng ca\n RCEEEeeeeeee!'
>
> Hint 2: docker? df -h

Truy cập trang web là mình nhận được source code

![image-20230619173803696](./assets/image-20230619173803696.png)

Format lại và decode các chuỗi đã encode trong mã cho dễ nhìn :D

```php
<?php
goto TwWdv;
kP1Xc:
if ($url["scheme"] !== "http" && $url["scheme"] !== "https") {
    die;
}
goto dD_At;
B10vf:
function curl($url)
{
    $ch = curl_init($url);
    _:
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $ua = "User-Agent: " . ($_GET["ua"] ?? "vinhjaxt/1.0 (Mô vina Zai Zóc Vơ sần 69.0.1 nhanh tuyệt cú mèo chấn động năm châu, tương thích oép 5 CHẤM 0, nhanh đóng băng hỏa diệm sơn, với tốc độ ánh sáng bờ nốc chên)");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array($ua));
    curl_setopt($ch, CURLOPT_URL, $url);
    $d = curl_exec($ch);
    $redirect_url = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
    $url = $redirect_url;
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpcode >= 300 && $httpcode < 400 && $redirect_url) {
        goto _;
    }
    curl_close($ch);
    return $d;
}
goto zUkQD;
SZSj0:
_:
goto WuY9_;
RzWUE:
$ch = curl_init();
goto YlbNQ;
dD_At:
if ($url["host"] === "127.0.0.1" || gethostbyname($url["host"]) === "127.0.0.1") {
    die;
}
goto RzWUE;
TwWdv:
show_source(__FILE__);
goto B10vf;
zUkQD:
$url = parse_url($_GET["url"]);
goto kP1Xc;
YlbNQ:
if (curl_escape($ch, $_GET["url"]) === urlencode($_GET["url"])) {
    die;
}
goto SZSj0;
WuY9_:
echo curl($_GET["url"]);
```

Đại loại là bài này sử dụng `curl` đến một `url` được lấy từ param `$_GET["url"]`, trước khi curl thì url của mình có qua các bước kiểm tra đầu vào

```php
if ($url["scheme"] !== "http" && $url["scheme"] !== "https") {
    die;
}
if ($url["host"] === "127.0.0.1" || gethostbyname($url["host"]) === "127.0.0.1") {
    die;
}
if (curl_escape($ch, $_GET["url"]) === urlencode($_GET["url"])) {
    die;
}
```

Mình cần đảm bảo các điều kiện này để chương trình có thể curl đến url của mình. Mình có tìm hiểu về sự khác nhau của `curl_escape` và `urlencode`, cả 2 đều có mục đích là url encode nhưng mà 2 cái này khác nhau ở chỗ `curl_escape` thì không encode một vài kí tự trong đó có dấu `+`. Vậy nên khi đó url của mình sẽ là 

```
http://103.163.25.143:20109/?url=http://1:1+1@9vrvdr2e.requestrepo.com
```

Kết quả curl sẽ được hiển thị trên màn hình

![image-20230621204652142](./assets/image-20230621204652142.png)

Mà trong source code có ghi là nó sẽ curl một lần nữa nếu trang web có redirect url

```php
    $redirect_url = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
    $url = $redirect_url;
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($httpcode >= 300 && $httpcode < 400 && $redirect_url) {
        goto _;
    }
```

Thử sửa lại response redirect đến một file hệ thống

![image-20230621205201085](./assets/image-20230621205201085.png)

Kết quả thành công -> vậy nên ở challenge này là lỗi Directory Traversal, mình cần tìm flag ở một đường dẫn nào đó

![image-20230621205228308](./assets/image-20230621205228308.png)

Khi hint 2 được đưa ra thì mình dự đoán flag được mount vào container trong docker vậy nên có thể có đường dẫn flag nằm trong `/proc/mounts` của container

![image-20230621205415470](./assets/image-20230621205415470.png)

Đọc file `/proc/mounts` mình tìm được đường dẫn flag `/home/siuvip_saoanhbatduocem/etc/passwd`

![image-20230621205554608](./assets/image-20230621205554608.png)

Đọc file `/home/siuvip_saoanhbatduocem/etc/passwd` và mình có flag

![image-20230621205633736](./assets/image-20230621205633736.png)

```
Flag: KMACTF{Pha?i_the'_chu'! La`m to't le'm con troai!}
```
# Flag Holder

![image-20230619140718231](./assets/image-20230619140718231.png)

> :warning:
>
> Hint 1: Server-side Template Injection liệu có khả thi ??? Hay còn một cách nào khác ????? 🤔🤔🤔🤔🤔 
>
> Hint 2: `if word in string.lower()[:MAX_LENGTH]:`

Giao diện trang web là một form điền template và variable tương ứng

![image-20230619141302970](./assets/image-20230619141302970.png)

Sau khi submit thì trả về một trang web render với template và variable đã điền

![image-20230619141343478](./assets/image-20230619141343478.png)

Challenge này cho mình source code ở `/source` (Ctrl + U ở trang đầu tiên là thấy)

```python
from flask import Flask, request, render_template_string, render_template, make_response
import os

app = Flask(__name__)
FLAG = os.getenv("FLAG") 
MAX_LENGTH = 20

def waf(string):
    blacklist = ["{{", "_", "'", "\"", "[", "]", "|", "eval", "os", "system", "env", "import", "builtins", "class", "flag", "mro", "base", "config", "query", "request", "attr", "set", "glob", "py"]
    for word in blacklist:
        if word in string.lower()[:MAX_LENGTH]:
            return False
    return True

@app.route('/')
def hello():
    return render_template("index.html")

@app.route("/render", methods = ["GET"])
def render():
    template = request.args.get("template")
    variable = request.args.get("variable")
    if len(template) == 0 or len(variable) == 0:
        return "Missing parameter required"
    if len(template) > MAX_LENGTH or len(variable) > MAX_LENGTH:
        return "Input too long"
    if not waf(template) or not waf(variable):
        return "Try harder broooo =)))"
    data = template.replace("{FLAG}", FLAG).replace("{variable}", variable)
    return render_template_string(data)

@app.route("/source", methods = ["GET", "POST"])
def source():
    response = make_response(open("./app.py", "r").read(), 200)
    response.mimetype = "text/plain"
    return response

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)

```

Như trong source code đã cho thì mình thu thập được một vài thông tin, và mình dự đoán trang web này có lỗ hổng SSTI. 

Sau khi lấy dữ liệu từ các param thì chương trình có thực hiện một số bước kiểm tra

```python
	// kiểm tra độ dài > 0
	if len(template) == 0 or len(variable) == 0: 
        return "Missing parameter required"
    // đảm bảo độ dài của param <= 20
    if len(template) > MAX_LENGTH or len(variable) > MAX_LENGTH: 
        return "Input too long"
    // thực hiện kiểm tra qua hàm waf()
    if not waf(template) or not waf(variable):
        return "Try harder broooo =)))"
    // hàm waf()
    def waf(string):
        blacklist = ["{{", "_", "'", "\"", "[", "]", "|", "eval", "os", "system", "env", "import", "builtins", "class", "flag", "mro", "base", "config", "query", "request", "attr", "set", "glob", "py"]
        for word in blacklist:
            if word in string.lower()[:MAX_LENGTH]:
                return False
        return True
```

Có thể thấy trong hàm `waf()` đã blacklist kha khá các class và cả bao gồm chuỗi `flag` nữa. Mà độ dài của các param phải `0 < param <= 20` nên khá là khó để thực hiện SSTI

```python
    data = template.replace("{FLAG}", FLAG).replace("{variable}", variable)
    return render_template_string(data)
```

Sau khi kiểm tra đầu vào thì chương trình thực hiện replace `replace("{FLAG}", FLAG)` và `replace("{variable}", variable)`, nhưng mà `flag` đã bị blacklist rồi nên mình không thể render nó ra được

Mình đã mò một hồi khá lâu mà chỉ có thể trigger SSTI được như này thôi :). Vì độ dài này không đủ để cho mình thực hiện attack được :)

![image-20230619142533174](./assets/image-20230619142533174.png)

Sau khi Hint 1 được đưa ra thì mình đã bỏ qua trường hợp SSTI và chuyển hướng qua tìm bug khác ở trong bài này :D

Đọc kĩ từng dòng code và mình thấy dòng `if word in string.lower()[:MAX_LENGTH]:` là dòng có khả năng nhất thôi :DD. Liệu rằng... có một kí tự nào đó khi lower lại có độ dài > 1 hay không vì nó kiểm tra trong độ dài `[:20]`, trong trường hợp này nó sẽ bị tràn và khả năng không có kiểm tra được các kí tự ở cuối

Tìm kí tự này

```python
for i in range(10000):
    if len(chr(i).lower()) > 1:
        print(i)
// output: 304, kí tự này là: İ
```

URL encode và gửi request

```
GET /render?template=%C4%B0%C4%B0%C4%B0%C4%B0%C4%B0%C4%B0%C4%B0%C4%B0{FLAG}&variable=longkd719 HTTP/1.1
Host: 103.163.25.143:20105
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/114.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
Referer: http://103.163.25.143:20105/
Upgrade-Insecure-Requests: 1


```

Kết quả

![image-20230619143237535](./assets/image-20230619143237535.png)

`Flag: KMACTF{WAF_1s_s0_5tR0nG_BuT_pYth0n_1s_s0_H4rd}`
# Ninja Shop

![image-20230621205729971](./assets/image-20230621205729971.png)

Trang web trả về một login form, có cả chức năng register

![image-20230625145217208](./assets/image-20230625145217208.png)

Mình thử reg 1 account và login thử thì trang web trả về một trang `Ninja Shop` 

![image-20230625145338755](./assets/image-20230625145338755.png)

`Buy Flag` thì trả về `Try harder!!! =))) ` :v

![image-20230625145445926](./assets/image-20230625145445926.png)

Còn các Ninja còn lại thì đồng giá 1 xu

![image-20230625145523377](./assets/image-20230625145523377.png)

Challenge cho source code như sau

```
.
├── Dockerfile
├── db
│   └── db.sql
├── docker-compose.yml
├── php.ini
└── src
    ├── config.php
    ├── imgs
    │   ├── flag.png
    │   ├── kakashi.png
    │   ├── naruto.png
    │   └── sasuke.png
    ├── index.php
    ├── login.php
    ├── profile.php
    └── register.php

3 directories, 13 files
```

Challenge này sử dụng MySQL để thực hiện các thao tác với database

Trong file `config.php` có khởi tạo một hàm `waf()` kiểm tra đầu vào để phòng chống sqli

```php
function waf($input) {
    // Prevent sqli -.-
    $blacklist = join("|", ["sleep", "benchmark", "order", "limit", "exp", "extract", "xml", "floor", "rand", "count", "or" ,"and", ">", "<", "\|", "&","\(", "\)", "\\\\" ,"1337", "0x539"]);
    if (preg_match("/${blacklist}/si", $input)) die("<strong>Stop! No cheat =))) </strong>");
    return TRUE;
}
```

Ở phần register và login thì đều có thể sqli được ở username, giới hạn độ dài của username là 26 và fullname là 10

```php
// register.php

<?php require 'config.php'; ?>

<?php
    if (!empty($_SESSION['username']) && !empty($_SESSION['uid']) )
        header('Location: index.php');
    
    if ( $_SERVER['REQUEST_METHOD'] !== "POST" )
        die(<<<EOF
        ... HTML ...
        EOF);

    foreach (array("username", "password", "fullname") as $value) {
        if (empty($_POST[$value])) die("<aside>Missing parameter!</aside>");
        waf($_POST[$value]);
    }
    if (strlen($_POST["username"]) > 26) die("<aside>Username too long<aside>");
    if (strlen($_POST["fullname"]) > 10) die("<aside>Fullname too long<aside>");
    
    // check account is exists
    if ( $connection->query(sprintf('SELECT * FROM users WHERE username="%s" and password="%s"', $_POST["username"], md5($_POST["password"])))->fetch_assoc()["uid"] )
        die("<strong>User is exists</strong>");

    $result = $connection->query(sprintf('INSERT INTO users(username, password, fullname) VALUES ("%s", "%s", "%s")', $_POST["username"], md5($_POST["password"]), $_POST["fullname"]));

    if ($result) {
        $uid = $connection->query(sprintf('SELECT uid FROM users WHERE username="%s" and password="%s" limit 0,1', $_POST["username"], md5($_POST["password"])))->fetch_assoc()["uid"];
        if ($uid) {
            if ($connection->query(sprintf('INSERT INTO coins(coin, uid) VALUES (100, %d)', (int)$uid)))
                die("<strong>User created successfully</strong>");
        }
    } else die("<strong>Something went wrong! Try again!</strong>");
?>
    
// login.php
<?php require 'config.php'; ?>

<?php
    if (!empty($_SESSION['username']) && !empty($_SESSION['uid']) )
        header('Location: index.php');

        if ( $_SERVER['REQUEST_METHOD'] !== "POST" )
        die(<<<EOF
            ... HTML ...
            EOF
        );
    
    foreach (array("username", "password") as $value) {
        if (empty($_POST[$value])) die("<aside>Missing parameter!</aside>");
        waf($_POST[$value]);
    }

    if (strlen($_POST["username"]) > 26) die("<aside>Username too long<aside>");
    $result = $connection->query(sprintf('SELECT * FROM users WHERE username="%s" AND password="%s" limit 0,1', $_POST["username"], md5($_POST["password"])))->fetch_assoc();
    if(empty($result))
        die(<<<EOF
            <strong>Username or password is wrong</strong>
            <meta http-equiv="refresh" content="1;url=login.php" />
        EOF);
    else {
        $_SESSION["username"] = $result['username'];
        $_SESSION["uid"] = $result['uid'];
        die(<<<EOF
            <strong>Welcome brooo!! Buy flag with me !!!! </strong>
            <meta http-equiv="refresh" content="1;url=index.php" />
        EOF);
    }

?>

```

Tại `index.php` cho mình biết được là mình cần có 1337 xu để có được `flag`

```php
<?php require 'config.php'; ?>

<?php
if (empty($_SESSION['username']) || empty($_SESSION['uid']) )
    die(<<<EOF
        <meta http-equiv="refresh" content="0;url=login.php" />
    EOF);

$ninjas = ["naruto", "sasuke", "kakashi", "flag"];
if ( isset($_GET["buy"]) ) {
    if (!in_array($_GET["buy"], $ninjas)) die("<aside>Product not available yet</aside>");
    $coin = $connection->query(sprintf("SELECT coin FROM coins WHERE uid=%d limit 0,1", (int)$_SESSION["uid"]))->fetch_assoc()["coin"];
    if ($_GET["buy"] === "flag") {
        if ( (int)$coin === 1337 /*(int)$coin > 1337 */  ) {        // must be 1337 :v
            $connection->query(sprintf("UPDATE coins SET coin=%d-1337 WHERE uid=%d", (int)$coin, (int)$_SESSION["uid"]));
            die("<strong>Nice brooo!! Are you a millionaire??? Here your flag: $FLAG</strong>");
        }
        else die("<strong>Try harder!!! =))) </strong>");
    }
    else {
        if ((int)$coin > 1) {
            $result = $connection->query(sprintf("UPDATE coins SET coin=%d-1 WHERE uid=%d", (int)$coin, (int)$_SESSION["uid"]));
            if ($result) die(sprintf("<strong>Buy successfully!!! Your coin: %d", (int)$coin - 1));
        }
        else die("<strong>Coin do not enough!! =(( </strong>");
    }
}
?>
```

`profile.php` là trang hiển thị thông tin của user, có chức năng update coin thông qua `GET["new_balance"]`, giá trị mới này có giới hạn độ dài là 2 và phải pass được qua hàm `waf()`

```php
<?php require 'config.php'; ?>

<h1>Profile</h1>
<?php
    if (empty($_SESSION['username']) || empty($_SESSION['uid']) )
        die(<<<EOF
            <meta http-equiv="refresh" content="0;url=login.php" />
        EOF);
    
    $fullname = $connection->query(sprintf("SELECT fullname FROM users WHERE username='%s' limit 0,1", $_SESSION["username"]));
    if (gettype($fullname) !== "boolean") echo "<h2>Hello: ". $fullname->fetch_assoc()["fullname"] ."</h2>";
    else echo "<h2>Hello: Anonymous </h2>";
    $coin = $connection->query(sprintf("SELECT * FROM coins WHERE uid=%d limit 0,1", (int)$_SESSION["uid"]))->fetch_assoc()["coin"];
    echo <<<EOF
        <h2>Your coin: $coin</h2>
    EOF;

    // Ran out of money?? No need to worry, you can reset carefree!! But only limited from 1-99 coins =)))
    if ( isset($_GET["new_balance"]) and waf($_GET["new_balance"]) ) {
        if (strlen($_GET["new_balance"]) > 2) die("<strong>Only allow from 1 to 99</strong>");
        else {
            $result = $connection->query(sprintf("UPDATE coins SET coin=%s WHERE uid=%d", $_GET["new_balance"], (int) $_SESSION['uid']));
            if ($result) die("<strong>Your coin has been updated</strong>");
            else die("<strong>0ops!!! Coin update has failed</strong>");
        }
    } 
?>
```

![image-20230625194614518](./assets/image-20230625194614518.png)

Flag là một biến môi trường, không hề nằm trong database, vậy nên mục tiêu của mình chỉ có thể là khiến cho số coin của mình = 1337 hoặc lớn hơn xíu bởi vì hàm `waf()` đã có filter `1337`, để đạt được mục đích này thì mình cần phải inject vào database bằng một cách nào đó rồi đổi giá trị coin của mình

Tại các phần register và login thì mình cũng đã thử inject được và thành công nhưng mình lại không biết cách để làm sao update được số coin của mình, tại đây mình chỉ có thể inject để login và register thành công thôi chứ không còn làm gì được khác

![image-20230625195909969](./assets/image-20230625195909969.png)

Mình cũng thử tìm ở các trang khác thì có trang `profile.php` cũng có thể inject được ở phần số coin

![image-20230625200034743](./assets/image-20230625200034743.png)

Nhưng mà độ dài ở đây giới hạn là 2 nên mình cũng không làm gì được hơn nữa. 

Sau khi mình xin thêm hint của anh trai @nhienit thì mình đã biết rằng mình đã bỏ qua một số thứ đáng chú ý, cụ thể là ở trang `profile.php` có thực hiện câu query này

```php
$fullname = $connection->query(sprintf("SELECT fullname FROM users WHERE username='%s' limit 0,1", $_SESSION["username"]));
```

Ban đầu mình cũng không để ý cho lắm nhưng mà ở đây có một sự bất thường là `username='%s'` sử dụng kí tự `'` khác với các trang register và login là `"`. Đây chính là lỗi `Second-order SQL injection`, về lỗ hổng này xảy ra khi mình inject một đoạn query nhưng không thực thi ở câu query này mà lại thực thi ở câu query khác. Cụ thể hơn là nếu như mình reg một account có username là `a'#` thì khi login thì chương trình vẫn nhận `a'#` là một user hợp lệ, nhưng khi vào trang profile thì lại xảy ra lỗi SQLI vì khi này câu query của mình có dạng

```sql
SELECT fullname FROM users WHERE username='a'#' limit 0,1
```

Vậy nên lúc này chương trình sẽ select fullname của user có `username='a'` chứ không phải là `a'#` như mình đã đăng kí ở trên. Oke vậy giờ mình cần phải làm gì khi mà mình chỉ có thể update coin với 2 kí tự :D ?

Mình stuck đoạn này khá lâu và được gợi ý cách sử dụng một variable có giá trị mình cần :D, rồi sau đó sử dụng biến này để set cho coin bởi vì tại câu query set coin sử dụng `%s` nên mình có thể làm được điều này

```sql
UPDATE coins SET coin=%s WHERE uid=%d
```

Tạo một user có username là một câu query khởi tạo biến `@z:=1339`

![image-20230625201353973](./assets/image-20230625201353973.png)

Login

![image-20230625201439055](./assets/image-20230625201439055.png)

Set coin bằng biến `@z` -> thành công 

![image-20230625201457041](./assets/image-20230625201457041.png)

Không có Sakura nên mình tạm mua Naruto với giá 1 xu để giảm coin xuống 1337 🤐

![image-20230625201618433](./assets/image-20230625201618433.png)

Mua flag :D

![image-20230625201636139](./assets/image-20230625201636139.png)

`Flag: KMACTF{Bruhhhhhh!!Lmaoooooo!!m4ke_SQL1_gr34t_4g4in!!!!!}`




{% endraw %}